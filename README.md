# bvptograf

Flash the code onto a ESP-01 and connect it to your Nibe Fighter 1235 and you will get all parameters published to your favourite MQTT-server.

## Details
Update User and Password credentials to your local Wifi and MQTT-server in the credentials.h-file. Compile and upload the code to the ESP-01.
Connect an RS485 transceiver to RX, TX and GPIO0(Direction).
Note that the ESP-01 only works with 3.3V voltage levels.

If you like to have nice graphs you can install InfluxDB and Grafana on a host computer in your network.
In glue_script you can find a python-script that subscribes to some selected Topics and push them to the Influx databse. The database content can then be nicely presented by Grafana.

Mosquitto, InfluxDB and Grafana can easily be co-located on a Raspberry Pi Zero W.


## Remote control the heatpump
It is also possible to write parameters to the heatpump by sendin a UDP-package to the ESP-01 on port 9898 with a three byte payload.
<address_high><address_low><new_setting>

<address_high> is always 0.

Some interesting addresses:

- 11 : heatcurve slope
- 12 : heatcurve offset
- 67 : heatcurve offset by RCU

Note that to remotely change the heatcurve offset, address 67 must be used since the controlpanel is controling address 12. Remember that the ESP-01 act as an RCU.
See doc/parameters_nibe.txt for more parameters. Not all are RW. Also check the file nibeParameters.c.


## Heatpump smart control
A very simple python-script is included in the glue_script-folder, nibeSmartControl.py

The script will read the following,

- Current heatcurve slope from heatpump
- Current RCU heatcurve offset from the heatpump
- Current outside temperature from heatpump outdoor sensor
- Current weather forcast (temp and wind-speed) for the coming 6 hours

From those parameters a RCU heatcurve offset is calculated and updated if needed.
The script is intended to be executed once every hour as a cron-job. No point in higher frequency since the weather forcast (SMHI) is not updated more frequent.

Copy the script to /usr/local/bin/ and update your crontab,
50 * * * *      /usr/local/bin/nibeSmartControl.py

The "smart" in nibeSmartControl.py is that it calculates a new outside temperature that is compensated for the windchill effect. It also look ahead and try to meet the coming temperature changes in advance. Check the lookahead-variable in the script. Set it to the amount of hours that fit your house. 3 hours seem good for my brick house.
