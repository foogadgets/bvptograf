#include <stdint.h>
#include "nibeDataParser.h"
#include "nibeParameters.h"


// Interpret and save parameters in msg.
// Every parameter is saved in an unsigned long for convenience.
// Returns the number of detected parameters.
//
byte parseAndStore(unsigned long int* res, const byte* const msg, const byte len) {
  uint8_t payloadLen;
  byte params = 0;

  for (uint8_t i = 0; i < (len); i++) { // Iterate over parameter data

    if (0x00 != msg[i]) return 0x00;  // First address byte should always be 0x00
    i++;
    params++; // Keeps track of amount of parameters read
    int testInt = msg[i];

    switch (testInt)
    {
      case 0:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 11:
      case 12:
      case 13:
      case 14:
      case 15:
      case 17:
      case 21:
      case 22:
      case 23:
      case 24:
      case 25:
      case 32:
      case 40:
      case 41:
      case 45:
      case 47:
      case 48:
      case 49:
      case 50:
      case 51:
      case 52:
      case 53:
      case 54:
      case 56:
      case 58:
      case 59:
      case 61:
      case 62:
      case 63:
      case 64:
      case 65:
      case 66:
      case 74:
      case 75:
      case 76:
      case 77:
      case 78:
      case 79:
      case 80:
      case 85:
      case 86:
      case 87:
      case 88:
      case 89:
      case 90:
      case 91:
      case 92:
      case 96:  // 8-bit integer parameters
        i++;
        res[testInt] = (msg[i] << 0);
        setNibeTopicValue( testInt, (int8_t)res[testInt] );
        break;

      case 1:
      case 9:
      case 10:
      case 16:
      case 18:
      case 19:
      case 20:
      case 26:
      case 27:
      case 28:
      case 29:
      case 30:
      case 34:
      case 35:
      case 36:
      case 37:
      case 38:
      case 39:
      case 42:
      case 43:
      case 44:
      case 55:
      case 57:
      case 60:
      case 67:
      case 68:
      case 69:
      case 70:
      case 71:
      case 72:
      case 73:
      case 81:
      case 82:
      case 83:
      case 84:
      case 93:
      case 94:
      case 95:  // 16-bit integer parameters
        i++;
        res[testInt] = ((msg[i] << 8) | (msg[i + 1] << 0));
        setNibeTopicValue( testInt, (int16_t)res[testInt] );
        i += 1;
        break;

      case 8:
      case 31:
      case 33:
      case 46:  // 32-bit integer parameters
        i++;
        res[testInt] = ((msg[i] << 24) | (msg[i + 1] << 16) | (msg[i + 2] << 8) | (msg[i + 3] << 0));
        setNibeTopicValue( testInt, ((int32_t)res[testInt] ));
        i += 3;
        break;

      default:
        return 0x00;  // Shold never happen with Nibe Fighter 1235
        break;
    }
  }
  return params;
}


// Return XOR-checksum
byte calcChecksum(const byte* const msg, const uint8_t len) {
  byte checksum = 0;
  for (uint8_t i = 0; i < len; i++) checksum ^= msg[i];
  return checksum;
}
