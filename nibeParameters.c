#include <stdint.h>
#include "nibeParameters.h"

topic nibeTopic[NOOFPARAMS];

int setNibeTopicValue(const int index, const unsigned long value) {
  nibeTopic[index].value = value;
  return 0;
}

unsigned long int getNibeTopicValue(const int index) {
  return nibeTopic[index].value;
}

char* getNibeTopic(const int index) {
  return nibeTopic[index].name;
}

int initNibeTopics(void) {
  for (int i = 0; i < NOOFPARAMS; i++) {
    nibeTopic[i].value, 0x00000000;
  }

  sprintf(nibeTopic[0].name,  "Nibe1235/Produkt");
  sprintf(nibeTopic[1].name,  "Nibe1235/Vattenmanteltemp");
  sprintf(nibeTopic[2].name,  "Nibe1235/Start_varmvatten");  //RW
  sprintf(nibeTopic[3].name,  "Nibe1235/Stopp_varmvatten");  //RW
  sprintf(nibeTopic[4].name,  "Nibe1235/Stopp_extra_varmvatten");  //RW
  sprintf(nibeTopic[5].name,  "Nibe1235/Stopp_kpr_extra varmvatten");  //RW
  sprintf(nibeTopic[6].name,  "Nibe1235/Intervall_periodiskt XVV");  //RW
  sprintf(nibeTopic[7].name,  "Nibe1235/Drifttid_varmvatten_(m)");
  sprintf(nibeTopic[8].name,  "Nibe1235/Drifttid_varmvatten_(h)");
  sprintf(nibeTopic[9].name,  "Nibe1235/Framledningstemp");
  sprintf(nibeTopic[10].name, "Nibe1235/Beraknad_framledningstemp");
  sprintf(nibeTopic[11].name, "Nibe1235/Kurvlutning");  //RW
  sprintf(nibeTopic[12].name, "Nibe1235/Forskjutning_varmekurva");  //RW
  sprintf(nibeTopic[13].name, "Nibe1235/Mintemp_framledning");  //RW
  sprintf(nibeTopic[14].name, "Nibe1235/Maxtemp_framledning");  //RW
  sprintf(nibeTopic[15].name, "Nibe1235/Kompensering_yttre"); //RW
  sprintf(nibeTopic[16].name, "Nibe1235/Returledningstemp");
  sprintf(nibeTopic[17].name, "Nibe1235/Max_returtemp");  //RW
  sprintf(nibeTopic[18].name, "Nibe1235/Gradminuter");
  sprintf(nibeTopic[19].name, "Nibe1235/Framledningstemp_2");
  sprintf(nibeTopic[20].name, "Nibe1235/Beraknad_framledningstemp_2");
  sprintf(nibeTopic[21].name, "Nibe1235/Kurvlutning_2");  //RW
  sprintf(nibeTopic[22].name, "Nibe1235/Forskjutning_varmekurva_2");  //RW
  sprintf(nibeTopic[23].name, "Nibe1235/Mintemp_framledning_2");  //RW
  sprintf(nibeTopic[24].name, "Nibe1235/Maxtemp_framledning_2");  //RW
  sprintf(nibeTopic[25].name, "Nibe1235/Kompensering_yttre_2");  //RW
  sprintf(nibeTopic[26].name, "Nibe1235/Returledningstemp_2");
  sprintf(nibeTopic[27].name, "Nibe1235/Utomhustemp");
  sprintf(nibeTopic[28].name, "Nibe1235/Medelutomhustemp");
  sprintf(nibeTopic[29].name, "Nibe1235/Koldbarare_in");
  sprintf(nibeTopic[30].name, "Nibe1235/Koldbarare_ut");
  sprintf(nibeTopic[31].name, "Nibe1235/Kompressorstarter");
  sprintf(nibeTopic[32].name, "Nibe1235/Drifttid_kompressor_(m)");
  sprintf(nibeTopic[33].name, "Nibe1235/Drifttid_kompressor_(h)");
  sprintf(nibeTopic[34].name, "Nibe1235/Hetgastemp");
  sprintf(nibeTopic[35].name, "Nibe1235/Vatskeledningstemp");
  sprintf(nibeTopic[36].name, "Nibe1235/Suggastemp");
  sprintf(nibeTopic[37].name, "Nibe1235/Temp_efter_kondensor");
  sprintf(nibeTopic[38].name, "Nibe1235/Rumstemp");
  sprintf(nibeTopic[39].name, "Nibe1235/Installd_rumstemp");
  sprintf(nibeTopic[40].name, "Nibe1235/Sommarlagetemp");  //RW
  sprintf(nibeTopic[41].name, "Nibe1235/Vinterlagetemp");  //RW
  sprintf(nibeTopic[42].name, "Nibe1235/Stromforbrukning_L1");
  sprintf(nibeTopic[43].name, "Nibe1235/Stromforbrukning_L2");
  sprintf(nibeTopic[44].name, "Nibe1235/Stromforbrukning_L3");
  sprintf(nibeTopic[45].name, "Nibe1235/Drifttid_tillsats_(m)");
  sprintf(nibeTopic[46].name, "Nibe1235/Drifttid_tillsats_(h)");
  sprintf(nibeTopic[47].name, "Nibe1235/Endast_el-tillsats");  //RW
  sprintf(nibeTopic[48].name, "Nibe1235/Shuntgrupp_2");  //RW
  sprintf(nibeTopic[49].name, "Nibe1235/Golvtork");  //RW
  sprintf(nibeTopic[50].name, "Nibe1235/Poolstyrning");  //RW
  sprintf(nibeTopic[51].name, "Nibe1235/Fabriksinstallning");  //RW
  sprintf(nibeTopic[52].name, "Nibe1235/Kylsystem");  //RW
  sprintf(nibeTopic[53].name, "Nibe1235/Driftlage");  //RW
  sprintf(nibeTopic[54].name, "Nibe1235/Extra_varmvatten");  //RW
  sprintf(nibeTopic[55].name, "Nibe1235/Kompensering_yttre_status");
  sprintf(nibeTopic[56].name, "Nibe1235/Rumskompensering");  //RW
  sprintf(nibeTopic[57].name, "Nibe1235/Multiregister_1"); // 2 bits 0x0200, 0x0400
  sprintf(nibeTopic[58].name, "Nibe1235/Multiregister_2"); // 7 bits 0x40, 0x01, 0x08, 0x10, 0x20, 0x80, 0x04
  sprintf(nibeTopic[59].name, "Nibe1235/Multiregister_3"); // 2 bits 0x01, 0x08
  sprintf(nibeTopic[60].name, "Nibe1235/Kyla_aktiv_(kylning_pagar)");
  sprintf(nibeTopic[61].name, "Nibe1235/RCU_kurvforskjutning_1"); // RW
  sprintf(nibeTopic[62].name, "Nibe1235/RCU_kurvforskjutning_2"); // RW
  sprintf(nibeTopic[63].name, "Nibe1235/Ingen_funktion_1");
  sprintf(nibeTopic[64].name, "Nibe1235/Ingen_funktion_2"); // RW
  sprintf(nibeTopic[65].name, "Nibe1235/Ingen_funktion_3"); // RW
  sprintf(nibeTopic[66].name, "Nibe1235/Ingen_funktion_4"); // RW
  sprintf(nibeTopic[67].name, "Nibe1235/Toppgivare_varmvatten");
  sprintf(nibeTopic[68].name, "Nibe1235/Pooltemperatur");
  sprintf(nibeTopic[69].name, "Nibe1235/ar");
  sprintf(nibeTopic[70].name, "Nibe1235/manad");
  sprintf(nibeTopic[71].name, "Nibe1235/dag");
  sprintf(nibeTopic[72].name, "Nibe1235/timme");
  sprintf(nibeTopic[73].name, "Nibe1235/minut");
  sprintf(nibeTopic[74].name, "Nibe1235/Nollstallning_larm"); // RW
  sprintf(nibeTopic[75].name, "Nibe1235/Snabbstart"); // RW
  sprintf(nibeTopic[76].name, "Nibe1235/Installd_pooltemp"); // RW
  sprintf(nibeTopic[77].name, "Nibe1235/Diff_pool"); // RW
  sprintf(nibeTopic[78].name, "Nibe1235/Kylkurva"); // RW
  sprintf(nibeTopic[79].name, "Nibe1235/Forskjutning_kylkurva"); // RW
  sprintf(nibeTopic[80].name, "Nibe1235/Utetemp_start_kyla"); // RW
  sprintf(nibeTopic[81].name, "Nibe1235/Startvarde_kompressor"); // RW
  sprintf(nibeTopic[82].name, "Nibe1235/Startvarde_tillsats"); // RW
  sprintf(nibeTopic[83].name, "Nibe1235/Sakringsstorlek");
  sprintf(nibeTopic[84].name, "Nibe1235/Max_antal_elsteg");
  sprintf(nibeTopic[85].name, "Nibe1235/Diff_tillsats"); // RW
  sprintf(nibeTopic[86].name, "Nibe1235/VB_diff_VP"); // RW
  sprintf(nibeTopic[87].name, "Nibe1235/Diff_VP-TS"); // RW
  sprintf(nibeTopic[88].name, "Nibe1235/Ingen_funktion_5");
  sprintf(nibeTopic[89].name, "Nibe1235/Golvtork_period_1_(dgr)"); // RW
  sprintf(nibeTopic[90].name, "Nibe1235/Golvtork_period_1_(max)"); // RW
  sprintf(nibeTopic[91].name, "Nibe1235/Golvtork_period_2_(dgr)"); // RW
  sprintf(nibeTopic[92].name, "Nibe1235/Golvtork_period_2_(max)"); // RW
  sprintf(nibeTopic[93].name, "Nibe1235/Ind_HPAC_Aktiv");
  sprintf(nibeTopic[94].name, "Nibe1235/Ind_HPAC_Passiv");
  sprintf(nibeTopic[95].name, "Nibe1235/Ind_periodiskt_XVV");
  sprintf(nibeTopic[96].name, "Nibe1235/Larm");
  sprintf(nibeTopic[97].name, "Nibe1235/VB_delta"); // VB_fram(9) - VB_retur(16)
  sprintf(nibeTopic[98].name, "Nibe1235/KB_delta"); // KB_in(29) - KB_ut(30)
  sprintf(nibeTopic[99].name, "Nibe1235/ESP8266_heap");

  return 0;
}
