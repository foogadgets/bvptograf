#ifndef _NIBEDATAPARSER_H
#define _NIBEDATAPARSER_H
#ifdef __cplusplus
extern "C" {
#endif

#include <Arduino.h>
#include <stdint.h>

byte parseAndStore(unsigned long int*, const byte* const, const byte);
byte calcChecksum(const byte* const, const uint8_t);

#ifdef __cplusplus
} // extern "C"
#endif
#endif /* _NIBEDATAPARSER_H */
