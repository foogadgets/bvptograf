#!/usr/bin/env python3
# coding=utf-8

# Script that regulates the heatpump based on SMHI forcast temperature.
# The default lookahead is 3 hours.
# The temperature values are wind chill compensated.


import datetime
import time
from influxdb import InfluxDBClient
import urllib3
import simplejson
import socket
import array
import copy



UDP_IP = "192.168.1.122"
UDP_PORT = int(9898)
lookahead = 3

latitude = 58.37476
longitude = 18.692532

influxDbIP = '192.168.1.116'
influxDbUser = 'user'
influxDbPass = 'passwd'
influxDb = 'homedb'
influxQuery1 = "SELECT * FROM \"Nibe1235/Utomhustemp\" GROUP BY * ORDER BY DESC LIMIT 1"
influxQuery2 = "SELECT * FROM \"Nibe1235/Kurvlutning\" GROUP BY * ORDER BY DESC LIMIT 1"
influxQuery3 = "SELECT * FROM \"Nibe1235/smart/Kurvjustering\" GROUP BY * ORDER BY DESC LIMIT 1"

forcastInterval={0, 1, 2, 3, 4, 5}
prognos = array.array('f', [0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
vind = array.array('f', [0.0, 0.0 ,0.0 ,0.0 ,0.0 ,0.0])
rcuKurvoffset = [0,61,0]

def writeInfluxData(progn, deltaT, justering, koldfaktor):
    writeTime = datetime.datetime.utcnow()
    json_body = [
        {
            "measurement": "Nibe1235/smart/smhiPrognos0",
            "time": writeTime,
            "fields": {
                "value": progn[0]
            }
        },
        {
            "measurement": "Nibe1235/smart/smhiPrognos1",
            "time": writeTime,
            "fields": {
                "value": progn[1]
            }
        },
        {
            "measurement": "Nibe1235/smart/smhiPrognos2",
            "time": writeTime,
            "fields": {
                "value": progn[2]
            }
        },
        {
            "measurement": "Nibe1235/smart/smhiPrognos3",
            "time": writeTime,
            "fields": {
                "value": progn[3]
            }
        },
        {
            "measurement": "Nibe1235/smart/smhiPrognos4",
            "time": writeTime,
            "fields": {
                "value": progn[4]
            }
        },
        {
            "measurement": "Nibe1235/smart/smhiPrognos5",
            "time": writeTime,
            "fields": {
                "value": progn[5]
            }
        },
        {
            "measurement": "Nibe1235/smart/deltaT",
            "time": writeTime,
            "fields": {
                "value": deltaT
            }
        },
        {
            "measurement": "Nibe1235/smart/Kurvjustering",
            "time": writeTime,
            "fields": {
                "value": justering
            }
        },
        {
            "measurement": "Nibe1235/smart/Koldfaktor",
            "time": writeTime,
            "fields": {
                "value": koldfaktor
            }
        }
    ]
    dbclient.write_points(json_body)


def twos(var):
    if (var<0):
        var=(1<<8)+(var)
    return (var)&0xff


def getSmhiTemp(lat,lon):
    url ='https://opendata-download-metfcst.smhi.se/api/category/pmp3g/version/2/geotype/point/lon/%s/lat/%s/data.json' % (lon,lat)
    http = urllib3.PoolManager()
    r = http.request('GET', url)
    result = simplejson.loads(r.data.decode('utf-8'))
    for x in forcastInterval:
        for findTemp in result['timeSeries'][x]['parameters']:
            if findTemp['name']=='t':
                prognos[x]=findTemp['values'][0]
            if findTemp['name']=='ws':
                vind[x]=findTemp['values'][0]


def setParameter(MESSAGE):
    MESSAGE=copy.deepcopy(MESSAGE)
    MESSAGE[2]=twos(MESSAGE[2])
    sock=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(bytearray(MESSAGE), (UDP_IP, UDP_PORT))


def calcOffset(hc, dT):
    if (dT>0):
        newParameter=(dT*hc*1.2/(hc+10)-0.25)
    else:
        newParameter=(dT*hc*1.2/(hc+10)+0.25)
    return round(newParameter)


def calcColdFactor(T, W):
    if (T>10 or W<2):
		print("No wind chill compensation done.")
        return T
    return round((13.12+0.6215*T-13.956*W**0.16+0.48669*T*W**0.16), 1)

# Set up a client for InfluxDB
dbclient = InfluxDBClient(influxDbIP, 8086, influxDbUser, influxDbPass, influxDb)
print("Connected to influx")


# Collect data and calculate
# Get latest local Nibe temperature
result=dbclient.query(influxQuery1)
points=result.get_points()
for item in points:
    nibeTemp=item["value"]
# Get latest Nibe heatcurve
result=dbclient.query(influxQuery2)
points=result.get_points()
for item in points:
    nibeHeatCurve=item["value"]
# Get latest Nibe offset curve
result=dbclient.query(influxQuery3)
points=result.get_points()
for item in points:
    nibeOffsetCurve=item["value"]
# Fetch SMHI forcast
getSmhiTemp(latitude, longitude)
# Calculate the delta to forcast
deltaTemp = nibeTemp-prognos[0]
deltaSmhi = calcColdFactor(prognos[0], vind[0])-calcColdFactor(prognos[lookahead], vind[lookahead])
# Calculate the offset
rcuKurvoffset[2] = calcOffset(nibeHeatCurve, deltaSmhi)
koldfaktor0 = calcColdFactor(prognos[0], vind[0])
koldfaktor3 = calcColdFactor(prognos[lookahead], vind[lookahead])



writeInfluxData(prognos, deltaSmhi, rcuKurvoffset[2], koldfaktor0)
if (nibeOffsetCurve!=rcuKurvoffset[2]):
    setParameter(rcuKurvoffset)


# Print collected and calculated data
print("nibeTemp: ",nibeTemp)
for x in forcastInterval:
    print("Prognos[%d]: %.1f\t  Vind[%d]: %.1f"%(x, prognos[x], x, vind[x]))
print("Delta-temperatur: %.2f"%deltaTemp)
print("Delta-SMHI: %.2f"%deltaSmhi)
print("HeatCurve: ", nibeHeatCurve)
print("Kurvoffset: ", rcuKurvoffset[2])
print("Köldfaktor0: %.1f"%(koldfaktor0))
print("Köldfaktor3: %.1f"%(koldfaktor3))
