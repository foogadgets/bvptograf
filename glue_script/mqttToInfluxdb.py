#!/usr/bin/env python3
# coding=utf-8

import paho.mqtt.client as mqtt
import datetime
import time
from influxdb import InfluxDBClient

influxDbIP = ''
influxDbUser = ''
influxDbPass = ''
influxDb = ''
mqttIP = ''


def on_connect(client, userdata, flags, rc):
    #print("Connected with result code "+str(rc))
    client.subscribe("Nibe1235/Vattenmanteltemp")
    client.subscribe("Nibe1235/Framledningstemp")
    client.subscribe("Nibe1235/Kurvlutning")
    client.subscribe("Nibe1235/Returledningstemp")
    client.subscribe("Nibe1235/Gradminuter")
    client.subscribe("Nibe1235/Utomhustemp")
    client.subscribe("Nibe1235/Hetgastemp")
    client.subscribe("Nibe1235/Suggastemp")
    client.subscribe("Nibe1235/Rumstemp")
    client.subscribe("Nibe1235/Installd_rumstemp")
    client.subscribe("Nibe1235/Multiregister_1")
    client.subscribe("Nibe1235/Multiregister_2")
    client.subscribe("Nibe1235/Multiregister_3")
    client.subscribe("Nibe1235/Antal_kompressorstarter")
    client.subscribe("Nibe1235/Drifttid_tillsats_(h)")
    client.subscribe("Nibe1235/Drifttid_kompressor_(h)")
    client.subscribe("Nibe1235/Toppgivare_varmvatten")
    client.subscribe("Nibe1235/Forskjutning_varmekurva")
    client.subscribe("Nibe1235/Beraknad_framledningstemp")
    client.subscribe("Nibe1235/Koldbarare_in")
    client.subscribe("Nibe1235/Koldbarare_ut")
    client.subscribe("Nibe1235/VB_delta")
    client.subscribe("Nibe1235/KB_delta")
    client.subscribe("Nibe1235/ESP8266_heap")
    client.subscribe("sensors/#")
    #client.subscribe("Nibe1235/Stromforbrukning_L1")
    #client.subscribe("Nibe1235/Stromforbrukning_L2")
    #client.subscribe("Nibe1235/Stromforbrukning_L3")

def on_message(client, userdata, msg):
    # Use utc as timestamp
    receiveTime = datetime.datetime.utcnow()
    timeNow = int(time.time())
    message = msg.payload.decode("utf-8")
    isfloatValue = False
    try:
        # Convert the string to a float so that it is stored as a number and not a string in the database
        val = float(message)
        isfloatValue = True
    except:
        print("Could not convert " + message + " to a float value")
        isfloatValue = False
    if isfloatValue:
        if 'energy' in msg.topic:
            json_body = [
                {
                    "measurement": msg.topic,
                    "time": receiveTime,
                    "fields": {
                        "value": val
                    }
                },
                {
                    "measurement": msg.topic.replace("energy","power"),
                    "time": receiveTime,
                    "fields": {
                        "value": rfxmeter_power(val, timeNow)
                    }
                }
            ]
        else:
            json_body = [
                {
                    "measurement": msg.topic,
                    "time": receiveTime,
                    "fields": {
                        "value": val
                    }
                }
            ]
        dbclient.write_points(json_body)


def rfxmeter_power(energyNow, theTime):
    global timeOld
    global energyOld
    power = 0
    if timeOld > 0 and energyOld < energyNow:
        power = (energyNow-energyOld)*3600/int(theTime-timeOld)
    energyOld = energyNow
    timeOld = theTime
    return int(power)



timeOld = 0
energyOld = 0

# Set up a client for InfluxDB
dbclient = InfluxDBClient(influxDbIP, 8086, influxDbUser, influxDbPass, influxDb)
print("Connected to influx")

# Initialize the MQTT client that should connect to the Mosquitto broker
client = mqtt.Client("mqtt2influx")
client.on_connect = on_connect
client.on_message = on_message
connOK=False
while(connOK == False):
    try:
        client.connect(mqttIP, 1883, 60)
        connOK = True
    except:
        connOK = False
    time.sleep(2)
print("Connected to mqtt")

# Blocking loop to the Mosquitto broker
client.loop_forever()
