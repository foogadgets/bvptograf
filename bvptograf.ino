/*
  Address:
  f9 = Display
  14 = RCU
  24 = Styrkort
  f5 = Reläkort

  Frame format:
       +----+----+
    => | 00 | 14 |
       +----+----+
       +----+
    <= | 06 |
       +----+
       +---------------------------+
       |            Header         |
       +-----+----+-------+--------+-------------+-------+
    => | CMD | 00 | Model | pkgLen | Payload ... | cksum |
       +-----+----+-------+--------+-------------+-------+

  Typical data after ACK-ing 00 014 from CPU.
  00 14 (06) C0 00 7c 10 00 00 7c 00 01 01 ee 00 02 2f 00 03 36 00 04 3c 1e

*/

extern "C" {
#include "user_interface.h"
}
#include <stdint.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiUdp.h>
#include <ESP8266mDNS.h>
#include "nibeDataParser.h"
#include "nibeParameters.h"
#include "credentials.h"

// enable debug printouts, listen printouts e.g. via netcat (nc -l -u 50000)
//#define ENABLE_DEBUG
#define CMDPORT  9898       // UDP port to send Nibe parameters to. Address 2B followed by data 1B, 2B or 4B
#define DEBUGPORT 50000     // Connect to see debug $> nc -lu nibercu.local 50000
#define SEND_INTERVAL 150   // Send udp every SEND_INTERVAL pkgburst sent on RS485
#define DIRECTION_PIN  0    // Direction change pin for RS485 port ESP-01 GPIO-0


// Control bytes
#define ETX 0x03
#define ENQ 0x05
#define ACK 0x06
#define NAK 0x15


// ######### VARIABLES #########
#ifdef ENABLE_DEBUG
byte verbose = 2;
char debug_buf[100];
#endif

// States used by state machine
enum e_state {
  STATE_CMD_RECEIVED,
  STATE_WAIT_START,
  STATE_WAIT_HEADER,
  STATE_READ_PAYLOAD,
  STATE_OK_MESSAGE_RECEIVED,
  STATE_CRC_FAILURE,
};


e_state state = STATE_WAIT_START;

IPAddress debug_target_ip(192, 168, 1, 101);  // target IP address where debug UDP packats are sent
ESP8266WiFiMulti WiFiMulti;
WiFiUDP udpCmd;   // Incoming UDP traffic object injecting new parameter settings to Nibe
WiFiClient mqttWiFiClient;
PubSubClient mqttClient(mqttWiFiClient);
#ifdef ENABLE_DEBUG
WiFiUDP udpDebug; // Outgoing UDP traffic object to send debug messages
#endif

byte cmdString[128] = {0xc0, 0x00, 0x14};
byte handshake[] = {0x00};
byte buffer[128];
unsigned long int result[NOOFPARAMS]; // Stores all parameter values
byte *bufferPayload = &buffer[4];     // Pointing to incoming data packet payload
uint8_t bufferIndex = 0;
uint8_t skipCounter = 0;    // parameter to keep track of udp transmission frequency.
bool cmdPending = false;    // Handling of command UDP packet has low priority



// ######### SETUP #########
void setup()  {
  pinMode(DIRECTION_PIN, OUTPUT);
  digitalWrite(DIRECTION_PIN, LOW);

  Serial.begin(19200, SERIAL_8N2);

  initNetConnect();

#ifdef ENABLE_DEBUG
  udpDebug.begin(DEBUGPORT);
#endif
  udpCmd.begin(CMDPORT);

  for (int j = 0; j < NOOFPARAMS; j++) result[j] = 0x0000;
  initNibeTopics();

  mqttClient.setServer(mqttServer, mqttPort);

#ifdef ENABLE_DEBUG
  if (verbose) {
    debugPrint("\nSetup done!\n");
  }
#endif
}



// ######### MAIN LOOP #########
void loop() {

  if (!mqttClient.connected()) mqttReconnect();
  mqttClient.loop();

  MDNS.update();

  if (cmdPending == false) {
    if (udpCmd.parsePacket() > 0) {
      buffer[3] = udpCmd.read(&buffer[4], 123);
      cmdPending = true;
    }
  }

  switch (state) {

    case STATE_CMD_RECEIVED:  // Interpret incoming command
      {
        byte c;
        while (!Serial.available());  // Await ACK of the ENQ
        c = Serial.read();
        if ( ACK != c ) {
          cmdPending = false;
          state = STATE_WAIT_START;
          break;
        }

        // Build the send buffer
        // 0xc0 0x00 0x14 LEN 0x00 0x0b 0x09 CKSUM
        buffer[0] = 0xC0;
        buffer[1] = 0x00;
        buffer[2] = 0x14;
        buffer[buffer[3] + 4] = calcChecksum(buffer, (buffer[3] + 4));

        // Here is where the magic happens. By sending byte per byte the command
        // will be received OK by the heatpump
        for (int i = 0; i < (buffer[3] + 5); i++) {
          sendCmdString(&buffer[i], 1);
        }

        while (!Serial.available());  // Await ACK of the CMD sent
        c = Serial.read();
        if ( ACK == c ) {
          handshake[0] = ETX;
          sendCmdString(handshake, 1);
        }
        cmdPending = false;
        state = STATE_WAIT_START;
      }
      break;


    case STATE_WAIT_START:  // Loop until a possible start package found
      {
        if (Serial.available() > 0) {
          if (Serial.read() == 0x00) {
            while (!Serial.available());
            if (Serial.read() == 0x14) {
              if (cmdPending) { // Send ENQ if a UDP packet is queued
                handshake[0] = ENQ;
                sendCmdString(handshake, 1);
                state = STATE_CMD_RECEIVED;
              } else {          // Or ACK to let HP know we are here
                handshake[0] = ACK;
                sendCmdString(handshake, 1);
                bufferIndex = 0;
                state = STATE_WAIT_HEADER;
              }
            }
          }
        }
      }
      break;

    case STATE_WAIT_HEADER:  // Read in the header and validate
      {
        while (!Serial.available());
        buffer[bufferIndex++] = Serial.read();

        if (bufferIndex == 3)
        {
          state = STATE_READ_PAYLOAD;
          if (buffer[0] != 0xC0 || buffer[1] != 0x00 || buffer[2] != 0x7c)
          {
            state = STATE_WAIT_START;
          }
        }
      }
      break;

    case STATE_READ_PAYLOAD:  // Header OK. Read the payload
      {
        // Incoming package example
        // C0 00 7c 10 00 00 7c 00 01 01 ee 00 02 2f 00 03 36 00 04 3c 1e
        while (!Serial.available());
        buffer[bufferIndex++] = Serial.read();
        uint8_t payLen = 5 + (uint8_t)(buffer[3]); // 21

        for (bufferIndex; bufferIndex < payLen; bufferIndex++)
        {
          while (!Serial.available());
          buffer[bufferIndex] = Serial.read();
        }
        state = STATE_OK_MESSAGE_RECEIVED;

        byte dataChecksum = calcChecksum(buffer, (payLen - 1));
        if ( dataChecksum != buffer[(payLen - 1)] )
        {
          state = STATE_CRC_FAILURE;
        }
      }
      break;

    case STATE_CRC_FAILURE: // CRC seem wrong. Ask package to be re-sent
      {
        handshake[0] = NAK;
        sendCmdString(handshake, 1);
        bufferIndex = 0;
        state = STATE_WAIT_HEADER;
      }
      break;

    case STATE_OK_MESSAGE_RECEIVED: // Let HP know the package got through
      {
        handshake[0] = ACK;
        sendCmdString(handshake, 1);

#ifdef ENABLE_DEBUG
        if (verbose > 2) {
          debugPrint("Message received\n");
        }
#endif
        // Parse the incoming data and store it in one big structure
        // Also add some more interesting metrics
        parseAndStore(result, bufferPayload, (buffer[3]));
        if (skipCounter++ >= SEND_INTERVAL) { // Send over udp sometimes
          result[(NOOFPARAMS - 1)] = system_get_free_heap_size();
          result[(NOOFPARAMS - 2)] = result[29] - result[30]; // KB_delta
          result[(NOOFPARAMS - 3)] = result[9] - result[16];  // VB_delta
          setNibeTopicValue( (NOOFPARAMS - 1), (uint32_t)result[NOOFPARAMS - 1] );
          setNibeTopicValue( (NOOFPARAMS - 2), (int16_t)result[NOOFPARAMS - 2] );
          setNibeTopicValue( (NOOFPARAMS - 3), (int16_t)result[NOOFPARAMS - 3] );
          skipCounter = 0;
          float factor = 1.0;
          for (int j = 1; j < NOOFPARAMS; j++) {
            switch (j) {
              case 1:
              case 9:
              case 10:
              case 16:
              case 19:
              case 20:
              case 26:
              case 27:
              case 28:
              case 29:
              case 30:
              case 34:
              case 35:
              case 36:
              case 37:
              case 38:
              case 39:
              case 42:
              case 43:
              case 44:
              case 56:
              case 67:
              case 68:
              case 77:
              case 97:
              case 98:
                factor = 10.0;
                break;
              default:
                factor = 1.0;
                break;
            }
            mqttClient.publish(nibeTopic[j].name, String((signed long int)getNibeTopicValue(j) / factor, 1).c_str());
          }
        }
        state = STATE_WAIT_START;
      }
      break;
  }
}


// ######### FUNCTIONS #########

// Send string to heat pump RS485
void sendCmdString(const byte* mess, size_t len) {
  digitalWrite(DIRECTION_PIN, HIGH);
  delayMicroseconds(52);
  Serial.write(mess, len);
  Serial.flush();
  //delayMicroseconds(52);  // Might be needed if no bias-resistors are used
  digitalWrite(DIRECTION_PIN, LOW);
}


#ifdef ENABLE_DEBUG
void debugPrint(const char* data) {
  udpDebug.beginPacket(debug_target_ip, DEBUGPORT);
  udpDebug.write(data);
  udpDebug.endPacket();
}
#endif

void initNetConnect()
{
  // We start by connecting to a WiFi network
  WiFiMulti.addAP(wifi_SSID, wifi_PASSWD);
  WiFi.hostname(myHostname);
  while (!MDNS.begin(myHostname));

  while (WiFiMulti.run() != WL_CONNECTED) {
    delay(500);
  }
}

void mqttReconnect(void)
{
  while (!mqttClient.connected()) {
    if (mqttClient.connect(mqtt_client_name, mqtt_user, mqtt_pass)) {
      mqttClient.publish("rcu/Status", "Connected");
    } else {
      delay(5000);
    }
  }
}
