#ifndef _NIBEPARAMETERS_H
#define _NIBEPARAMETERS_H
#ifdef __cplusplus
extern "C" {
#endif

#include <Arduino.h>
#include <stdint.h>

#define NOOFPARAMS 100


struct topic {
  char name[64];
  unsigned long int value;
};
typedef struct topic topic;


extern topic nibeTopic[NOOFPARAMS];
int setNibeTopicValue(const int, const unsigned long);
unsigned long int getNibeTopicValue(const int);
char* getNibeTopic(const int);
int initNibeTopics(void);


#ifdef __cplusplus
} // extern "C"
#endif

#endif /* _NIBEPARAMETERS_H */
